# Docker in Docker - Node

[Docker in Docker](https://hub.docker.com/_/docker) + [Nodejs](https://hub.docker.com/_/node) + [Nodejs Build Tools](https://hub.docker.com/_/node).

[This project deploys to docker hub](https://hub.docker.com/r/madisongrubb/dind-node).

## image tags:

* 6
* 8
* 10
* latest (always the newest version tag of this image)

## Adding Versions

Peruse the [official node builds](https://nodejs.org/dist/). Add the desired version to the node_versions.csv.
The first entry should be the full version to include, the second entry is the version of npm to include, the third entry is the version of yarn to include.

Run the image updater script:

```
bash build.sh
```

Commit your changes and make a pull request.
